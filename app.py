import os
from flask import Flask, g, session, redirect, request, url_for, jsonify, render_template
from requests_oauthlib import OAuth2Session
from pymongo import MongoClient
from functools import wraps
import os
import binascii
import asyncio

client = MongoClient('localhost', 27017)
db = client.BotList
pending = db.pending
bots = db.bots

OAUTH2_CLIENT_ID = ""
OAUTH2_CLIENT_SECRET = ""
OAUTH2_REDIRECT_URI = 'http://localhost:5000/callback'

API_BASE_URL = os.environ.get('API_BASE_URL', 'https://discordapp.com/api')
AUTHORIZATION_BASE_URL = API_BASE_URL + '/oauth2/authorize'
TOKEN_URL = API_BASE_URL + '/oauth2/token'

app = Flask(__name__, static_url_path='/static')
app.debug = False
app.config['SECRET_KEY'] = OAUTH2_CLIENT_SECRET

if 'http://' in OAUTH2_REDIRECT_URI:
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = 'true'

def require_auth(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        api_token = session.get('oauth2_token')
        if api_token is None:
            return redirect('/login')
        return f(*args, **kwargs)
    return wrapper

def token_updater(token):
    session['oauth2_token'] = token


def make_session(token=None, state=None, scope=None):
    return OAuth2Session(
        client_id=OAUTH2_CLIENT_ID,
        token=token,
        state=state,
        scope=scope,
        redirect_uri=OAUTH2_REDIRECT_URI,
        auto_refresh_kwargs={
            'client_id': OAUTH2_CLIENT_ID,
            'client_secret': OAUTH2_CLIENT_SECRET,
        },
        auto_refresh_url=TOKEN_URL,
        token_updater=token_updater)

def accept_bot(id):
    b = pending.find_one({'_id': int(id)})
    if b:
        adata = {
            '_id': int(b['_id']),
            'name': b['name'],
            'support_server': b['support_server'],
            'profile_url': b['profile_url'],
            'invite_url': b['invite_url'],
            'prefix': b['prefix'],
            'website': b['website'],
            'description': b['description'],
            'verified': b['verified'],
            'rep': int(b['rep']),
            'guilds': int(b['guilds']),
            'owner': b['owner'],
            'owner_name': b['owner_name']
        }
        db.bots.insert_one(adata)
        db.pending.delete_one({'_id': int(b['_id'])})

def update_guild_count(ID, guilds):
    c = db.bots.find_one({'_id': ID})
    if c:
        cdata = {
            '_id': int(c['_id']),
            'name': c['name'],
            'support_server': c['support_server'],
            'profile_url': c['profile_url'],
            'invite_url': c['invite_url'],
            'prefix': c['prefix'],
            'website': c['website'],
            'description': c['description'],
            'verified': c['verified'],
            'rep': int(c['rep']),
            'guilds': int(guilds),
            'owner': c['owner'],
            'owner_name': c['owner_name']
        }
        db.bots.delete_one({'_id': c['_id']})
        db.bots.insert_one(cdata)

def decline_bot(id):
    c = pending.find_one({'_id': id})
    if c:
        pending.delete_one({'_id': id})

@app.route('/login')
def index():
    scope = request.args.get(
        'scope',
        'identify')
    discord = make_session(scope=scope.split(' '))
    authorization_url, state = discord.authorization_url(AUTHORIZATION_BASE_URL)
    session['oauth2_state'] = state
    return redirect(authorization_url)


@app.route('/callback')
def callback():
    if request.values.get('error'):
        return request.values['error']
    discord = make_session(state=session.get('oauth2_state'))
    token = discord.fetch_token(
        TOKEN_URL,
        client_secret=OAUTH2_CLIENT_SECRET,
        authorization_response=request.url)
    session['oauth2_token'] = token
    current_user = discord.get(API_BASE_URL + '/users/@me').json()
    userdata = db.users.find_one({'_id': int(current_user['id'])})
    if not userdata['_id'] is None:
        veri = userdata['verified']
        descrip = userdata['description']
    else:
        veri = False
        descrip = 'I am very Mysterious.'
    pdata = {
        '_id': int(current_user['id']),
        'username': '{}#{}'.format(current_user['username'], current_user['discriminator']),
        'profile_url': "https://cdn.discordapp.com/avatars/{}/{}.png".format(current_user['id'], current_user['avatar']),
        'description': descrip,
        'api_token': userdata['api_token'],
        'verified': veri
    }
    db.users.delete_one({'_id': int(current_user['id'])})
    db.users.insert_one(pdata)
    return redirect(url_for('.home'))

@app.route('/bot/<int:ID>')
def bot(ID):
    data = db.bots.find_one({'_id': ID})
    if data:
        return render_template('bot.html', data=data)
    else:
        return "Bot does not exist.", 404

@app.route('/settings')
@require_auth
def settings():
    discord = make_session(token=session.get('oauth2_token'))
    user = discord.get(API_BASE_URL + '/users/@me').json()
    userdata = db.users.find_one({'_id': int(user['id'])})
    return render_template('settings.html', settings=userdata['description'], token=userdata['api_token'])

@app.route('/api/settings/update', methods=['GET'])
@require_auth
def settings_api():
    discord = make_session(token=session.get('oauth2_token'))
    current_user = discord.get(API_BASE_URL + '/users/@me').json()
    descript = request.headers['bio']
    userdata = db.users.find_one({'_id': int(current_user['id'])})
    data = {
        '_id': int(current_user['id']),
        'username': '{}#{}'.format(current_user['username'], current_user['discriminator']),
        'profile_url': "https://cdn.discordapp.com/avatars/{}/{}.png".format(current_user['id'], current_user['avatar']),
        'description': str(descript),
        'api_token': userdata['api_token'],
        'verified': False
    }
    db.users.delete_one({'_id': int(current_user['id'])})
    db.users.insert_one(data)
    return "Updated", 200
    
@app.route('/add')
@require_auth
def addbot():
    return render_template('add-bot.html')

@app.route('/bots')
def bots():
    bot = db.bots.find()
    return render_template('bots.html', bots=bot)

@app.route('/bots/query=<result>')
def bots_query(result):
    bot = db.bots.find({'name': result})
    return render_template('bots.html', bots=bot)

@app.route('/api/add', methods=['POST'])
@require_auth
def add_bot():
    discord = make_session(token=session.get('oauth2_token'))
    user = discord.get(API_BASE_URL + '/users/@me').json()
    id = request.form['client-id']
    name = request.form['bot-name']
    support = request.form['support-link']
    invite_url = request.form['invite-url']
    prefix = request.form['prefix']
    website = request.form['website-url']
    desc = request.form['description']
    profile_url = request.form['profile']
    data = {
        '_id': int(id),
        'name': name,
        'support_server': support,
        'profile_url': profile_url,
        'invite_url': invite_url,
        'prefix': prefix,
        'website': website,
        'description': desc,
        'verified': False,
        'rep': 0,
        'guilds': 0,
        'owner': int(user['id']),
        'owner_name': str(user['username'] + "#" + user['discriminator'])
    }
    count = len(desc)
    if count <= 250:
        pending.insert_one(data)
        return redirect('/'), 301
    else:
        return "Description is too long."

@app.route('/api/pending/decline/<int:id>', methods=['GET'])
@require_auth
def decline(id):
    decline_bot(id)
    return redirect('/admin/panel'), 301

@app.route('/api/pending/accept/<int:id>', methods=['GET'])
@require_auth
def accept(id):
    accept_bot(id)
    return redirect('/admin/panel'), 301

@app.route('/api/guilds/<int:ID>', methods=['POST'])
def guild_count(ID):
    robo = request.headers['token']
    guilds = request.headers['guilds']
    b = db.users.find_one({'api_token': str(robo)})
    bot = db.bots.find_one({'_id': ID})
    if str(b['_id']) == str(bot['owner']):
        update_guild_count(ID, guilds)
        return "Updated", 200
    else:
        return "Not Authorized"

@app.route('/api/rep/<int:id>', methods=['GET'])
@require_auth
def rep(id):
    discord = make_session(token=session.get('oauth2_token'))
    user = discord.get(API_BASE_URL + '/users/@me').json()
    r = db.rep.find_one({'_id': int(user['id'])})
    if not r:
        d = db.bots.find_one({'_id': id})
        new_rep = d['rep']
        new_rep += 1
        if d:
            cdata = {
                '_id': int(d['_id']),
                'name': d['name'],
                'support_server': d['support_server'],
                'profile_url': d['profile_url'],
                'invite_url': d['invite_url'],
                'prefix': d['prefix'],
                'website': d['website'],
                'description': d['description'],
                'verified': d['verified'],
                'rep': int(new_rep),
                'guilds': int(d['guilds']),
                'owner': int(d['owner']),
                'owner_name': d['owner_name']
            }
            db.bots.delete_one({'_id': id})
            db.rep.insert_one({'_id': int(user['id'])})
            db.bots.insert_one(cdata)
            return "Repped", 200
    else:
        return "Already repped."

@app.route('/admin/panel', methods=['GET'])
@require_auth
def pending_bots():
    discord = make_session(token=session.get('oauth2_token'))
    user = discord.get(API_BASE_URL + '/users/@me').json()
    if user['id'] == "296084893459283968" or "256962349217349632":   
        items = db.pending.find()
        return render_template('admin.html', items=items)
    else:
        return "not authorized"

@app.route('/logout')
def logout():
    session.clear()
    return redirect('/')

@app.route('/')
def home():
    items = db.featured.find()
    return render_template('home.html', bots=items)

@app.route('/api')
def api():
    return render_template('api.html')

@app.route('/user/<user>')
def profile(user):
    if user == "me":
        if session.get('oauth2_token') is None:
            return redirect('/login'), 200
        else:
            discord = make_session(token=session.get('oauth2_token'))
            current_user = discord.get(API_BASE_URL + '/users/@me').json()
            userinfo = db.users.find_one({'_id': int(current_user['id'])})
            bots_owned = db.bots.find({'owner': int(current_user['id'])})
            return render_template('profile.html', bots=bots_owned, userdata=userinfo)
    else:
        userinfo = db.users.find_one({'_id': int(user)})
        if userinfo is None:
            return "User doesn't exist."
        else:  
            bots_owned = db.bots.find({'owner': int(user)})
            return render_template('profile.html', bots=bots_owned, userdata=userinfo)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
